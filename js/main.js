// AJAX это технология которая позволяет работать с асинхронными операциями без перезагрузки страницы

const URL = "https://ajax.test-danit.com/api/swapi/films"

class StarWars {
  getFetch(url) {
    return fetch(url).then(response => response.json())
  }

  render() {
    this.getFetch(URL).then(response => {
      response.forEach(({
        name,
        episodeId,
        openingCrawl,
        characters
      }) => {
        const list = document.createElement("ul");
        const nameElem = document.createElement("li");
        const episodeElem = document.createElement("li");
        const openingElem = document.createElement("li");
        nameElem.textContent = name
        episodeElem.textContent = episodeId
        openingElem.textContent = openingCrawl
        const charactersLi =  document.createElement("li");
        const charactersList = document.createElement("ul");
        charactersLi.append(charactersList)
        characters.forEach(element => this.getFetch(element).then(data => {
          const charactersElem = document.createElement("li");
          charactersElem.textContent = data.name
          charactersList.append(charactersElem)
        }))

        list.append(nameElem, episodeElem, openingElem, charactersLi);

        document.body.append(list)


      })
    })
  }



}

const listCharacter = new StarWars();
listCharacter.render()








  










